#! /usr/bin/env python3

import json

class Charger:
    def __init__(self, name, conn):
        self.name = name
        self.conn = conn
        self.charging = False
        self.connection = "Connection.NONE"
        self.maxPower = 0
        self.power = 0

    def chargeAck(self):
        self.charging = True


    def broadcastConnected(self):
        print("sending connection")
        self.conn.sendall(json.dumps({"connection" : "connected", "name" : self.name}).encode())

    def broadcastUpdatePower(self):
        self.conn.sendall(json.dumps({"chargeCar" : "charging", "name" : self.name, "power": self.power}).encode())

    def initCharging(self, connection):
        self.charging = False
        self.power = 0
        self.connection = connection
        if connection == "Connection.PLUG":
            self.maxPower = 11
        elif connection == "Connection.SOCKET":
            self.maxPower = 22
        else:
            print(f"charging connection to recognized!")

    def stopCharging(self):
        self.charging = False
        self.power = 0
        self.maxPower = 0
        self.conn.sendall(json.dumps({"chargeCar" : "disconnected", "name" : self.name}).encode())