#! /usr/bin/env python3

import json
import socket
import time
import threading

from LocationManager import LocationManager

class Server:
    def __init__(self):
        self.stop = False
        self.success = False
        self.locationManager = LocationManager()
        self.initSocket()

    def __del__(self):
        self.stop = True
        self.socket.close()
        for thread in self.threads:
            thread.join()

    def initSocket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(('127.0.0.1', 65421))
        self.socket.listen()
        self.threads = []

        thread = threading.Thread(target=self.connectionHandler)
        thread.daemon = True
        thread.start()
        self.threads.append(thread)

    def connectionHandler(self):
        while not self.stop:
            try:
                conn, addr = self.socket.accept()
            except BaseException as e:
                print(f"Failed to accept new connection: {e}")
                print(f"exiting...")
                self.stop = True

            clientThread = threading.Thread(target=self.handleClientRequest, args=(conn,addr))
            clientThread.daemon = True
            clientThread.start()
            self.threads.append(clientThread)

    def handleClientRequest(self, conn, addr):
        while not self.stop:
            try:
                requestRaw = conn.recv(1024)
                if not requestRaw:
                    print(f"client diconnected")
                    # handle client disconnect
                    break
                request = json.loads(requestRaw.decode())
                print(f"received request: {request}")
                if "connection" in request and request["connection"] == "init":
                    self.handleInitConnection(conn, request)
                elif "chargeCar" in request:
                    self.handleChargeCar(conn, request)
            except BaseException as e:
                raise "Exceptions while handling client request: " + e
                #print(f"Exceptions while handling client request: {e}")

    def handleInitConnection(self, conn, request):
        if "location" in request and "name" in request:
            charger = self.locationManager.addChargerToLocation(request["location"], request["name"], conn)
            if charger:
                charger.broadcastConnected()
            else:
                conn.sendall(json.dumps({"connection" : "failed", "msg": "Failed to add charger"}).encode())

    def handleChargeCar(self, conn, request):
        if request["chargeCar"] == "requestCharge" and "connection" in request:
            self.locationManager.chargeStart(conn, request["connection"])
        elif request["chargeCar"] == "disconnect":
            self.locationManager.chargeStop(conn)
        elif request["chargeCar"] == "chargingACK":
            self.locationManager.chargeAck(conn)
