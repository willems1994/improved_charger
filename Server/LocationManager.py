#! /usr/bin/env python3

from Location import Location

class LocationManager:
    def __init__(self):
        self.locations = []
        self.locations.append(Location("A", 22))
        self.locations.append(Location("B", 44))

    def getLocation(self, loc):
        dest = None
        for location in self.locations:
            if location.name == loc:
                dest = location
                break
        return dest

    def getChargerLocation(self, conn):
        dest = None
        for location in self.locations:
            if location.getCharger(conn) != None:
                dest = location
                break
        return dest

    def addChargerToLocation(self, location, name, conn):
        dest = self.getLocation(location)

        if not dest:
            print(f"Location {location} doesnt exist!")
            return None

        return dest.addCharger(name, conn)

    def chargeStart(self, conn, connection):
        dest = self.getChargerLocation(conn)

        if not dest:
            return False
        
        return dest.chargerStartCharging(conn, connection)

    def chargeStop(self, conn):
        dest = self.getChargerLocation(conn)

        if not dest:
            return False

        return dest.chargerStopCharging(conn)

    def chargeAck(self, conn):
        dest = self.getChargerLocation(conn)

        if not dest:
            return False

        return dest.chargeAck(conn)
