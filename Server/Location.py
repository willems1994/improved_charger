#! /usr/bin/env python3

import socket
import threading

from Charger import Charger

import sys
sys.path.insert(0,'..')

from Connection import Connection

class Location:
    def __init__(self, name, capacity):
        self.name = name
        self.totalCapacity = capacity
        self.remainingCapacity = capacity
        self.chargers = []
        self.rebalanceAlgorithm = self.qwelloRebalance
        self.rebalanceThread = None
        self.ackEvent = threading.Event()

    # return charger by either name or connection
    def getCharger(self, id):
        if isinstance(id, socket.socket):
            search = "conn"
        elif isinstance(id, str):
            search = "name"

        for charger in self.chargers:
            if (search == "conn" and charger.conn == id) or (search == "name" and charger.name == id):
                return charger
        return None

    def addCharger(self, name, conn):
        # check if charger already exists
        charger = self.getCharger(name)
        if charger:
            print(f"charger already registered at location!")
            if conn != charger.conn:
                print(f"connection changed, updating connection")
                charger.conn = conn
                return charger

        charger = Charger(name, conn)
        self.chargers.append(charger)
        return charger

    def chargerStartCharging(self, conn, connection):
        charger = self.getCharger(conn)
        if not charger:
            print(f"Charger not registered, abort charging...")
            return False

        charger.initCharging(connection)
        maxPower = charger.maxPower

        print(f"{charger.name} requested amount: {maxPower}, remaing capacity: {self.remainingCapacity}")

        if maxPower <= self.remainingCapacity:
            self.remainingCapacity -= maxPower
            charger.power = maxPower
            charger.broadcastUpdatePower()
        else:
            self.rebalance(charger)

        return True

    def chargerStopCharging(self, conn):
        charger = self.getCharger(conn)
        if not charger:
            print(f"Charger not registered, ignoring...")
            return False

        self.remainingCapacity += charger.power
        charger.stopCharging()

        self.rebalance()

        return True

    def chargeAck(self, conn):
        charger = self.getCharger(conn)
        if not charger:
            print(f"Charger not registered, ignoring...")
            return False
        
        print(f"Acknowledged charging")
        self.ackEvent.set()
        charger.charging = True


    def checkMaxPowerPossible(self, activeChargers):
        totalMaxPower = 0.0
        for charger in activeChargers:
            totalMaxPower += charger.maxPower
        
        print(f"total requested power: {totalMaxPower}, we have {self.totalCapacity}")
        if totalMaxPower <= self.totalCapacity:
            for charger in activeChargers:
                charger.power = charger.maxPower
                charger.broadcastUpdatePower()
                return True
        return False


    def rebalance(self, newCharger = None):
        activeChargers = [ charger for charger in self.chargers if (charger.charging == True) ]
        if newCharger:
            activeChargers.append(newCharger)
        if len(activeChargers) == 0:
            return
        
        if not self.checkMaxPowerPossible(activeChargers):
            print("rebalance")
            print(type(activeChargers))
            print(len(activeChargers))
            self.rebalanceThread = threading.Thread(target=self.qwelloRebalance, args=(activeChargers,))
            self.rebalanceThread.start()

    def qwelloRebalance(self, activeChargers):
        # retrieve number of plugs and sockets
        numberOfPlugs = 0
        numberOfSockets = 0
        for charger in activeChargers:
            if charger.connection == "Connection.PLUG":
                numberOfPlugs += 1
            elif charger.connection == "Connection.SOCKET":
                numberOfSockets += 1
            else:
                print(f"Connection not recognized!")
        
        # try giving max to the lower capacity connections and balance the rest
        powerForSockets = (self.totalCapacity - (numberOfPlugs * 11)) / numberOfSockets
        if (powerForSockets >= 11):
            for charger in activeChargers:
                print(f"updating charger {charger.name}")
                if charger.connection == "Connection.PLUG":
                    charger.power = 11
                elif charger.connection == "Connection.SOCKET":
                    charger.power = powerForSockets
                charger.broadcastUpdatePower()
                self.ackEvent.wait()
        # else give everyone equal charge
        else:
            chargePerCharger = self.totalCapacity / (len(activeChargers))
            for charger in activeChargers:
                charger.power = chargePerCharger
                charger.broadcastUpdatePower()
                self.ackEvent.wait()


################################### ALTERNATIVE LOADBALANCERS #########################################

    def equalRebalance(self, activeChargers):
        powerPerCharger = self.totalCapacity / len(activeChargers)
        print(f"everyone is getting {powerPerCharger} power")
        
        for charger in activeChargers:
            charger.power = powerPerCharger
            charger.broadcastUpdatePower()

    def proportionalRebalance(self, activeChargers):
        totalMaxPower = 0.0
        for charger in activeChargers:
            totalMaxPower += charger.maxPower

        for charger in activeChargers:
            charger.power = (charger.maxPower / totalMaxPower) * self.totalCapacity
            charger.broadcastUpdatePower()