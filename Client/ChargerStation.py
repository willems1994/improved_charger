#! /usr/bin/env python3

import json
import socket
import time
import threading

from enum import Enum

import sys
sys.path.insert(0,'..')

from Connection import Connection

class NetworkState(Enum):
    ONLINE = 1
    OFFLINE = 2

class ChargerStation:
    def __init__(self, name, location):
        self.name = name
        self.location = location
        self.socket = None
        self.networkState = NetworkState.OFFLINE
        self.stop = False
        self.connection = Connection.NONE

        self.chargingPower = 0
        self.capacity = 0

        self.connectToServer() # sets self.socket

        self.chargeThread = None

        # start a thread to receive updates from server
        self.updateThread = threading.Thread(target=self.receiveUpdates)
        self.updateThread.daemon = True
        self.updateThread.start()

    def __del__(self):
        self.stop = True
        self.socket.close()
        self.updateThread.join()
        if not self.chargeThread:
            self.chargeThread.join()

    def handShakeServer(self):
        print(f"initiating handshake to server")
        self.socket.sendall(json.dumps({"connection" : "init", "name" : self.name, "location" : self.location}).encode())

    def connectToServer(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(('127.0.0.1', 65421))
        self.handShakeServer()

    def waitForConnection(self):
        while self.networkState == NetworkState.OFFLINE and not self.stop:
            try:
                updateRaw = self.socket.recv(1024)
                update = json.loads(updateRaw.decode())
                if update["connection"] == "connected":
                    print(f"succesfully connected to server")
                    self.networkState = NetworkState.ONLINE
            except BaseException as e:
                print(f"exception occured while waiting for connection confirmation: {e}")

    def receiveUpdates(self):
        self.waitForConnection()
        while not self.stop and self.networkState==NetworkState.ONLINE:
            try:
                updateRaw = self.socket.recv(1024)
                update = json.loads(updateRaw.decode())
                print(f"received update: {update}")
                if "chargeCar" in update:
                    self.handleChargeCar(update)
            except BaseException as e:
                print(f"exception occured while receiving update: {e}")
                self.networkState = NetworkState.OFFLINE

    def handleChargeCar(self, update):
        if self.connection == Connection.NONE:
            print(f"got an update for car charging but no car connected")
            return

        if update["chargeCar"] == "disconnected":
            self.connection = Connection.NONE
            return

        if update["chargeCar"] == "charging" and "power" in update:
            self.chargingPower = update["power"]
            if not self.chargeThread:
                self.chargeThread = threading.Thread(target=self.charging)
                self.chargeThread.daemon = True
                self.chargeThread.start()
            self.socket.sendall(json.dumps({"chargeCar" : "chargingACK", "power" : self.chargingPower}).encode())
            
                
    def connect(self, connection: Connection, capacity: int):
        if self.connection != Connection.NONE:
            print(f"Can't connect, is already connected")
            return
        
        self.connection = connection
        self.capacity = capacity
        self.socket.sendall(json.dumps({"chargeCar" : "requestCharge", "connection" : str(self.connection)}).encode())
        
    def disconnect(self):
        if self.connection == Connection.NONE:
            print(f"no connection, aborting disconnect...")

        self.socket.sendall(json.dumps({"chargeCar" : "disconnect" }).encode())


    def charging(self):
        batteryLevel = 0
        while True and self.connection != Connection.NONE:
            time.sleep(1)
            batteryLevel += self.chargingPower
            if batteryLevel > self.capacity:
                print(f"charging {self.name} finished!")
                self.disconnect()
                break
            print(f"{self.name} charged {self.chargingPower}, left to charge: {self.capacity - batteryLevel}")