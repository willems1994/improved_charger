#! /usr/bin/env python3

from ChargerStation import ChargerStation, Connection
import time
import sys

chargerStationA1 = ChargerStation(sys.argv[1], sys.argv[2])

if sys.argv[3] == "PLUG":
    connection = Connection.PLUG
elif sys.argv[3] == "SOCKET":
    connection = Connection.SOCKET
else:
    raise "connection not recognized!"

chargerStationA1.connect(connection, int(sys.argv[4]))
while True:
    time.sleep(1)
