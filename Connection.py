from enum import Enum

class Connection(Enum):
    NONE = 1
    PLUG = 2  # 11 KW
    SOCKET = 3  # 22 KW