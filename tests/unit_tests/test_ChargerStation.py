#! /usr/bin/env python3

from mock import Mock, call, patch
import pytest 
import json
import sys
import socket

sys.path.insert(0,'../../Client')

from ChargerStation import ChargerStation

@patch('socket.socket')
def test_ChargerStation(mockSocket):
    mockCreatedSocket = Mock()
    mockSocket.return_value = mockCreatedSocket

    mockCreatedSocket.connect.return_value = True
    mockCreatedSocket.recv.return_value = json.dumps({"connection" : "connected"}).encode()  

    chargerStation = ChargerStation("Station1", "A")

    assert chargerStation.connected == True