#! /usr/bin/env python3

from mock import Mock, call, patch, MagicMock
import pytest 
import json
import sys
import socket
import time

sys.path.insert(0,'../../Server')

from Server import Server

mockConn = Mock()

def handleAccept():
    for i in range(1):
        mockConn = MagicMock(name='socket.socket', spec=socket.socket)
        time.sleep(1)
        yield (mockConn, ['0.0.0.0', 1234])
    while True:
        time.sleep(1) # so I have a chance to kill the process before the OS becomes unresponsive
        yield socket.error()



@patch('socket.socket')
def test_Server(mockSocket):
    mockCreatedSocket = Mock()
    mockSocket.return_value = mockCreatedSocket

    mockCreatedSocket.accept.side_effect = handleAccept()

    mockConn.recv.return_value = json.dumps({"connection" : "init", "name" : "test_name", "location" : "A"}).encode()  

    # mockConn.recv.assert_called_with(1024)
    # mockConn.sendall.assert_called_with(json.dumps({"connection" : "connected"}).encode())

    # mockCreatedSocket.accept.assert_called()
    # mockConn.recv.assert_called()
    # mockConn.sendall.assert_called()

    server = Server()
    time.sleep(1)

    assert server.success == True

